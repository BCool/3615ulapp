const pkg = require('./package')
import firebaseConfig from './config/firebase-config'
process.noDeprecation = true

module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    script: [
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js'
      }
    ],
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/icon.png' }]
  },

  manifest: {
    name: '3615ULApp',
    short_name: 'ULApp',
    theme_color: '#737a7e',
    background_color: '#cfd5da',
    display: 'standalone',
    scope: '/',
    start_url: '/',
    lang: 'fr'
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/ga.js', ssr: false },
    '@/plugins/plugins',
    '@/plugins/app-components'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/font-awesome',
    'bootstrap-vue/nuxt',
    [
      'nuxt-fontawesome',
      {
        component: 'fa',
        imports: [
          //import whole set
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          }
        ]
      }
    ],
    'nuxt-fire',
    '@nuxtjs/pwa',
    '@nuxtjs/robots',
    '@nuxtjs/google-analytics',
    [
      'nuxt-i18n',
      {
        seo: false,

        locales: [
          {
            code: 'fr',
            name: 'Français',
            iso: 'fr-FR',
            file: 'fr-FR.json'
          },
          {
            code: 'en',
            iso: 'en-US',
            name: 'English',
            file: 'en-US.json'
          }
        ],
        lazy: true,
        langDir: 'locales/',
        strategy: 'prefix_and_default',
        defaultLocale: 'fr'
      }
    ]
  ],

  env: {
    snackTimeoutMin: 3000, //minimum 3 secondes screening
    snackTimeoutMilisecPerCaractere: 100 //Calcul of the screening duration per caracteres
  },

  /**
   * nuxt-fire module -> https://www.npmjs.com/package/nuxt-fire
   */
  fire: firebaseConfig,

  googleAnalytics: {
    id: 'UA-146673682-1'
  },

  buildModules: ['@nuxtjs/vuetify'],

  vuetify: {
    optionsPath: './vuetify.options.js'
  },

  robots: {
    /* module options */
  },

  workbox: {
    dev: false
    // importScripts: ['custom-sw.js']
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
