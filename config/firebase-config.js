// doc -> https://www.npmjs.com/package/nuxt-fire
export default {
  useOnly: [
    'auth',
    'firestore',
    // 'realtimeDb',
    'functions',
    'storage'
  ],
  // customEnv: true, //
  config: {
    development: {
      apiKey: 'AIzaSyCVHSojLj2nlmUE8RkF2W-ASaIK4r-UqwU',
      authDomain: 'ulproject-fa7ed.firebaseapp.com',
      databaseURL: 'https://ulproject-fa7ed.firebaseio.com',
      projectId: 'ulproject-fa7ed',
      storageBucket: 'ulproject-fa7ed.appspot.com',
      messagingSenderId: '1903091817',
      appId: '1:1903091817:web:ca97b23f4cf406637582f8'
    },
    production: {
      apiKey: 'AIzaSyCVHSojLj2nlmUE8RkF2W-ASaIK4r-UqwU',
      authDomain: 'ulproject-fa7ed.firebaseapp.com',
      databaseURL: 'https://ulproject-fa7ed.firebaseio.com',
      projectId: 'ulproject-fa7ed',
      storageBucket: 'ulproject-fa7ed.appspot.com',
      messagingSenderId: '1903091817',
      appId: '1:1903091817:web:ca97b23f4cf406637582f8'
    }
  }
}
