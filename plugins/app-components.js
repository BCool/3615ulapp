import Vue from 'vue'

import AppSnackBar from '~/components/widgets/AppSnackbar'

Vue.component('AppSnackBar', AppSnackBar)

import browserDetect from 'vue-browser-detect-plugin'
Vue.use(browserDetect)

import VueLogger from 'vuejs-logger'
const isProduction = process.env.NODE_ENV === 'production'

const options = {
  isEnabled: true,
  logLevel: isProduction ? 'error' : 'debug',
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: '|',
  showConsoleColors: true
}

Vue.use(VueLogger, options)
