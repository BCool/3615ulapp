import Vue from 'vue'
import pathify from 'vuex-pathify'
pathify.options.mapping = 'simple'
export const plugins = [pathify.plugin]

//state
export const state = () => ({
  thumbs: { up: 0, down: 0 },
  snackTimeout: process.env.snackTimeoutMin,
  snackBar: false,
  snackX: 'right', // right or left
  snackY: 'top', // top or bottom
  snackMode: 'multi-line', //multi-line or vertical
  snackColor: undefined,
  snackText: ''
})

// getters
export const getters = {}

//mutations
export const mutations = {
  thumbs(state, thumbs) {
    state.thumbs = thumbs
  },

  resetNotif(state) {
    state.snackBar = false
  },

  notif(state, payload) {
    let timeout =
      payload.text.length * process.env.snackTimeoutMilisecPerCaractere
    if (payload.timeout === false) {
      state.snackTimeout = 0
    } else if (state.snackTimeout < timeout) {
      state.snackTimeout = timeout
    } else {
      state.snackTimeout = process.env.snackTimeoutMin
    }
    state.snackText = payload.text
    state.snackColor = payload.color
    state.snackMode = $nuxt.$vuetify.breakpoint.mdAndUp
      ? payload.text.length > 60
        ? 'multi-line'
        : null
      : 'vertical'
    state.snackBar = true
  },

  snackBar(state, value) {
    state.snackBar = value
  },

  snackX(state, value) {
    state.snackX = value
  },

  snackY(state, value) {
    state.snackY = value
  },

  snackMode(state, value) {
    state.snackMode = value
  }
}

// actions
export const actions = {
  goTo(context, payload) {
    if (typeof payload === 'string') {
      $nuxt.$router.push($nuxt.localePath(payload))
    } else {
      let { name, query, params, hash } = payload
      $nuxt.$router.push({
        path: $nuxt.localePath({ name, params, query }),
        hash: hash
      })
    }
  },

  notif({ state, commit }, payload) {
    if (state.snackBar) {
      setTimeout(() => {
        commit('notif', payload)
      }, 300)
    } else {
      commit('notif', payload)
    }
  },

  logError({ dispatch }, payload) {
    if (process.env.isDev) {
      $nuxt.$log.error(payload, payload.message)
    }
    dispatch('notifError', payload.message)
  },

  notifError({ state, dispatch }, payload) {
    dispatch('notif', {
      text: process.env.isDev ? payload : $nuxt.$t('global.problem'),
      color: 'red'
    })
  }
}
